package bookle.controlador;

import java.io.File;
import java.util.Date;
import java.util.LinkedList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.datatype.XMLGregorianCalendar;

import bookle.tipos.Actividad;
import bookle.tipos.ObjectFactory;
import bookle.tipos.TipoAgenda;
import bookle.tipos.TipoTurno;

public class Controlador implements BookleControlador {

	@Override
	public String createActividad(String titulo, String descripcion, String profesor, String email)
			throws BookleException {
		String id = Utils.createId();
		ObjectFactory of = new ObjectFactory();
		Actividad actividad = of.createActividad();
		actividad.setId(id);
		actividad.setTitulo(titulo);
		actividad.setDescripcion(descripcion);
		actividad.setProfesor(profesor);
		actividad.setEmail(email);
		JAXBContext contexto;
		try {
			contexto = JAXBContext.newInstance("bookle.tipos");
			Marshaller marshaller = contexto.createMarshaller();
			marshaller.setProperty("jaxb.formatted.output", true);
			marshaller.marshal(actividad, new File("actividades/"+id+".xml"));
		} catch (Exception e) {
			throw new BookleException("Error al guardar la actividad", e);
		}
		return id;
	}

	@Override
	public void updateActividad(String id, String titulo, String descripcion, String profesor, String email)
			throws BookleException {
		    JAXBContext contexto;
		    Actividad actividad;
			try {
				contexto = JAXBContext.newInstance("bookle.tipos");
				Unmarshaller unmarshaller = contexto.createUnmarshaller();
				actividad = (Actividad) unmarshaller.unmarshal(new File("actividades/"+id+".xml"));
				actividad.setTitulo(titulo);
				actividad.setDescripcion(descripcion);
				actividad.setProfesor(profesor);
				actividad.setEmail(email);
				Marshaller marshaller = contexto.createMarshaller();
				marshaller.setProperty("jaxb.formatted.output", true);
				marshaller.marshal(actividad, new File("actividades/"+id+".xml"));
			} catch (Exception e) {
				throw new BookleException("Error al cargar la actividad", e);
			}
			
	}

	@Override
	public Actividad getActividad(String id) throws BookleException {
		try {
			JAXBContext contexto = JAXBContext.newInstance("bookle.tipos");
			Unmarshaller unmarshaller = contexto.createUnmarshaller();
			Actividad actividad = (Actividad) unmarshaller.unmarshal(new File("actividades/"+id+".xml"));
			return actividad;
		} catch (Exception e) {
			throw new BookleException("Error al cargar la actividad", e);
		}
		
	}

	@Override
	public boolean removeActividad(String id) throws BookleException {
		File file = new File("actividades/" + id + ".xml");
		file.delete();
		if (file.exists()){
			return false;
		}
		return true;
	}

	@Override
	public void addDiaActividad(String id, Date fecha, int turnos) throws BookleException {
		try {
			JAXBContext contexto = JAXBContext.newInstance("bookle.tipos");
			Unmarshaller unmarshaller = contexto.createUnmarshaller();
			Actividad actividad = (Actividad) unmarshaller.unmarshal(new File("actividades/"+id+".xml"));
			TipoAgenda agenda = new TipoAgenda();
			agenda.setFecha(Utils.createFecha(fecha));
			for (int i=0;i<turnos;i++){
				agenda.getTurno().add(new TipoTurno());
			}
			actividad.getAgenda().add(agenda);
			Marshaller marshaller = contexto.createMarshaller();
			marshaller.setProperty("jaxb.formatted.output", true);
			marshaller.marshal(actividad, new File("actividades/"+id+".xml"));
		} catch (Exception e) {
			throw new BookleException("Error al cargar la actividad", e);
		}
		
		
	}

	@Override
	public boolean removeDiaActividad(String id, Date fecha) throws BookleException {
		boolean eliminado = false;
		try {
			JAXBContext contexto = JAXBContext.newInstance("bookle.tipos");
			Unmarshaller unmarshaller = contexto.createUnmarshaller();
			TipoAgenda agenda1 = null;
			
			Actividad actividad = (Actividad) unmarshaller.unmarshal(new File("actividades/"+id+".xml"));
			for (TipoAgenda agenda : actividad.getAgenda()) {
				if (Utils.dateFromString(agenda.getFecha().toString()).compareTo(fecha) == 0){
					agenda1 = agenda;
				}
			}
			eliminado = actividad.getAgenda().remove(agenda1);
			Marshaller marshaller = contexto.createMarshaller();
			marshaller.setProperty("jaxb.formatted.output", true);
			marshaller.marshal(actividad, new File("actividades/"+id+".xml"));
			
		} catch (JAXBException e) {
			throw new BookleException("Error al eliminar la actividad", e);
		}
		return eliminado;
	}

	@Override
	public int addTurnoActividad(String id, Date fecha) throws BookleException {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void removeTurnoActividad(String id, Date fecha, int turno) throws BookleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void setHorario(String idActividad, Date fecha, int indice, String horario) throws BookleException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public String createReserva(String idActividad, Date fecha, int indice, String alumno, String email)
			throws BookleException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean removeReserva(String idActividad, String ticket) throws BookleException {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public LinkedList<Actividad> getActividades() throws BookleException {
		// TODO Auto-generated method stub
		return null;
	}

	
}
