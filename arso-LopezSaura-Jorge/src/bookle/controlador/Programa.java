package bookle.controlador;

import java.util.Date;

import bookle.tipos.Actividad;

public class Programa {
	
	public static void main(String[] args) {
		Controlador controlador = new Controlador();
		String id = controlador.createActividad("titulo","descripcion","profesor","email");
		controlador.updateActividad(id,"titulo1", "descripcion1","profesor1","email1");
		Actividad actividad = controlador.getActividad(id);
		System.out.println("Actividad: Titulo: "+actividad.getTitulo()+" Descripcion: "+actividad.getDescripcion()+
				" Profesor: "+actividad.getProfesor()+" Email: "+actividad.getEmail());
		
		Date date = Utils.dateFromString("2018-03-20");
		Date date2 = Utils.dateFromString("2019-02-01");
		controlador.addDiaActividad(id,date,5);
		controlador.addDiaActividad(id,date2,10);
		controlador.removeDiaActividad(id, date);
		//controlador.removeActividad(id);
		
	}

}
