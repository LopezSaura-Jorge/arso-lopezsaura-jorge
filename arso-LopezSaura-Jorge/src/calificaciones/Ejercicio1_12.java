package calificaciones;


import java.io.File;
import java.math.BigDecimal;
import java.math.BigInteger;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

public class Ejercicio1_12 {
	
	public static void main(String[] args) throws JAXBException, DatatypeConfigurationException {
		//JAXBContext contexto = JAXBContext.newInstance("calificaciones");
		Calificaciones calificaciones = new Calificaciones();
		calificaciones.setConvocatoria(TipoConvocatoria.FEBRERO);
		calificaciones.setCurso(new BigInteger("2018"));
		calificaciones.setAsignatura("1092");
		TipoCalificacion calificacion = new TipoCalificacion();
		calificacion.setNif("23322156M");
		calificacion.setNota(new BigDecimal("10"));
		
		TipoCalificacion calificacion1 = new TipoCalificacion();
		calificacion1.setNif("13322156M");
		calificacion1.setNombre("Pepe");
		calificacion1.setNota(new BigDecimal("8"));
		
		calificaciones.getCalificacion().add(calificacion);
		calificaciones.getCalificacion().add(calificacion1);
		
		TipoDiligencia diligencia = new TipoDiligencia();
		diligencia.setNif("13322156M");
		diligencia.setNota(new BigDecimal("9"));
		XMLGregorianCalendar fecha =
				DatatypeFactory.newInstance().newXMLGregorianCalendar();
		fecha.setYear(2019);
		fecha.setMonth(02);
		fecha.setDay(12);
		
		calificaciones.getDiligencia().add(diligencia);
		
		// Empaquetado en un documento XML (marshalling)
		JAXBContext contexto = JAXBContext.newInstance("calificaciones");
		
		Marshaller marshaller = contexto.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", true);
		marshaller.marshal(calificaciones, new File("xml/salidaCalificaciones.xml"));
		
		System.out.println("Fin");
	}

}
