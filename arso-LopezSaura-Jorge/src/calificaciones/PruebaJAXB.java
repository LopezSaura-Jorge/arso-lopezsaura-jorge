package calificaciones;

import java.io.File;
import java.math.BigDecimal;
import java.util.Calendar;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class PruebaJAXB {
	public static void main(String[] args) throws JAXBException {
		JAXBContext contexto = JAXBContext.newInstance("calificaciones");
		Unmarshaller unmarshaller = contexto.createUnmarshaller();
		Calificaciones calificaciones =
				(Calificaciones) unmarshaller.unmarshal(new File("ejercicio1-3/ejercicio1_3.xml"));
		
		//Ejercicio 1.4
		int numDiligencias = 0;
		Calendar calendario = Calendar.getInstance(); // hoy
		calendario.add(Calendar.DAY_OF_MONTH,-30);
		for (TipoDiligencia diligencia :calificaciones.getDiligencia()) {
			if (diligencia.getFecha().toGregorianCalendar().getTime().after(calendario.getTime())){
				numDiligencias++;
			}
		}
		System.out.println("El numero de diligencias en 30 dias es: "+numDiligencias);	
		//Ejercicio 1.5
		int numNotas= 0;
		double sumaNotas = 0;
		
		for (TipoCalificacion calificacion : calificaciones.getCalificacion() ) {
			sumaNotas += calificacion.getNota().doubleValue();
			numNotas++;
		}
		System.out.println("La media de las notas de las calificaciones es: "+sumaNotas/numNotas);
		//Ejercicio 1.7
		
		for (TipoCalificacion calificacion : calificaciones.getCalificacion()) {
			double nota = calificacion.getNota().doubleValue();
			if (nota + 0.5 <= 10){
				nota += 0.5;
				calificacion.setNota(new BigDecimal(nota));
			}
		}
		// Empaquetado en un documento XML (marshalling)
		Marshaller marshaller = contexto.createMarshaller();
		marshaller.setProperty("jaxb.formatted.output", true);
		marshaller.setProperty("jaxb.schemaLocation",
				"http://www.example.org/ejercicio1_3 ejercicio1_3.xsd");
		marshaller.marshal(calificaciones, new File("xml/calificacionesJAXB.xml"));
		
	}

}
