package dom;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;


public class PruebaDOM {
	
	public static void main(String[] args) throws Exception {
		
		double mediaNotas = 0;
		int numNotas = 0;
		// 1. Obtener una factoría
		DocumentBuilderFactory factoria =DocumentBuilderFactory.newInstance();
		// 2. Pedir a la factoría la construcción del analizador
		// 3. Analizar el documento
		DocumentBuilder analizador;
		analizador = factoria.newDocumentBuilder();
		Document documento = analizador.parse("ejercicio1-3/ejercicio1_3.xml");
		
		Element calificaciones = documento.getDocumentElement();
		NodeList elementos = calificaciones.getElementsByTagName("nota");
		for (int i=0; i< elementos.getLength();i++){
			Element nota = (Element) elementos.item(i);
			if (nota.getParentNode().getNodeName().equals("calificacion")){
				String notaTexto = nota.getTextContent();
				double nota1 = Double.parseDouble(notaTexto);
				mediaNotas += nota1; 
				numNotas++;
				if (nota1 + 0.5 <=10){
					nota1+= 0.5;
					nota.setTextContent(Double.toString(nota1));
				}
			}
		}
		
		System.out.println("La media de notas de las calificaciones es: "+mediaNotas/numNotas);
		
		// 1. Construye la factoría de transformación y obtiene un
		// transformador
		TransformerFactory tFactoria = TransformerFactory.newInstance();
		Transformer transformacion = tFactoria.newTransformer();
		// 2. Establece la entrada, como un árbol DOM
		Source input = new DOMSource(documento);
		// 3. Establece la salida, un fichero en disco
		Result output = new StreamResult("xml/calificaciones-modificado.xml");
		// 4. Aplica la transformación
		transformacion.transform(input, output);
	}

}
