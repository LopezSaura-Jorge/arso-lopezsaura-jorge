package jaxb.manual;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement (namespace = "http://www.um.es/poema")
@XmlAccessorType(XmlAccessType.FIELD)
public class Poema {

	@XmlElement (required = true)
	private String titulo;
	@XmlAttribute (required =true)
	private String lugar;
	@XmlAttribute (required =true)
	private String fecha;
	private List<String> verso = new LinkedList<String>();
	
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public String getLugar() {
		return lugar;
	}
	public void setLugar(String lugar) {
		this.lugar = lugar;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public List<String> getVerso() {
		return verso;
	}
	public void setVerso(List<String> verso) {
		this.verso = verso;
	}
	
	
}
