package jaxb.manual;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

public class Programa {

	public static void main(String[] args) throws JAXBException {
		JAXBContext contexto = JAXBContext.newInstance(Poema.class);
		Poema poema = new Poema();
		
		poema.setTitulo("Alba");
		poema.setLugar("Granada");
		poema.getVerso().add("Mi corazon oprimido");
		poema.getVerso().add("siente junti a la alborada");
		
		//Validacion 
		SchemaFactory factoriaSchema =

				SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

		// Construye el esquema
		//Schema esquema = factoriaSchema.newSchema(new File(ficheroEsquema));
		// Solicita al esquema la construcción de un validador
		//Validator validador = esquema.newValidator();
		// Registra el manejador de eventos de error
		//7validador.setErrorHandler(new DefaultHandler() {
			//public void error(SAXParseException e) throws SAXException {
				//System.out.println("Error: " + e.getMessage());
				// throw e; -> para detener la validación
			//}
		//});
		// Solicita la validación de los objetos JAXB
		//validador.validate(new JAXBSource(contexto, calificaciones));
		
		

	}
}
