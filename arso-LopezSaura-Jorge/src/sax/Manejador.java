package sax;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class Manejador extends DefaultHandler{
	
	/*
	 * Atributos
	 * 
	 * 
	 * Los atributos son necesarios para:
	 * - Recordar el estado del procesamiento (dentroElemento1, etc.)
	 * - Almacenar el resultado del procesamiento. Se ofrecer� un m�todo
	 * de consulta para obtener el resultado.
	 * 
	 */
	private int numDiligencias;
	private boolean dentroDiligencia;
	private boolean dentroFecha;
	private boolean dentroCalificacion;
	private boolean dentroNota;
	private double mediaNotas;
	private int numNotas;
	/*
	 * Nota: cuando el estado del procesamiento es complejo, puede resultar �til
	 * usar una pila, como por ejemplo, LinkedList<String>
	 * 
	 * - En el evento startElement:
	 * pila.push(qName);
	 * 
	 * - En el evento endElement:
	 * pila.pop();
	 * 
	 * - En el evento characters obtenemos el elemento al que pertenece el texto:
	 * String elemento = pila.peek();
	 * 
	 */
	public int getNumDiligencias() {
		return numDiligencias;
	}
	
	public double getNotaMedia(){
		if (numNotas > 0){
			return mediaNotas/numNotas;
		}
		else
			throw new IllegalStateException("El documento no tiene notas");
		
	}
	
	@Override
	public void startDocument() throws SAXException {
		
		/*
		 * Cuando el an�lisis no es simple y requiere atributos,
		 * este m�todo est� a cargo de resetar los atributos.
		 * 
		 * Esto permite que un mismo objeto pueda ser utilizado en varios
		 * an�lisis.
		 */
		numDiligencias = 0;
		dentroDiligencia = false;
		dentroFecha = false;
		dentroCalificacion = false;
		dentroNota = false;
		mediaNotas = 0;
		numNotas = 0;
		
	}
	
	
	@Override
	public void startElement(String uri, String localName, 
			String qName, Attributes attributes) throws SAXException {
		
		/*
		 * Habitualmente en este evento se realizan dos tareas:
		 * - Obtener informaci�n de los atributos del elemento (attributes).
		 * - Poner a verdadero los atributos que llevan el estado 
		 * del procesamiento. Ejemplo:
		 * 
		 * if (qName.equals("elemento1")) dentroElemento1 = true;
		 * 
		 * Alternativamente, si se utiliza una pila:
		 * 
		 * pila.push(qName);
		 * 
		 */
		if (qName.equals("diligencia")){
			dentroDiligencia = true;
		}
		if (qName.equals("fecha")){
			dentroFecha = true;
		}
		if (qName.equals("calificacion")){
			dentroCalificacion = true;
		}
		if (qName.equals("nota")){
			dentroNota = true;
		}
		
	}
	
	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		
		String texto = new String(ch, start, length);
		
		/*
		 * En este evento se recupera el texto que envuelve un elemento.
		 * Para conocer de que elemento se trata, debemos consultar los
		 * atributos de estado o la cima de la pila: 
		 * 
		 * String elemento = pila.peek()
		 */
		if (dentroDiligencia && dentroFecha){
			SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
			Date fecha = null;
			try {
				fecha = formato.parse(texto);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Calendar calendario = Calendar.getInstance(); // hoy
			calendario.add(Calendar.DAY_OF_MONTH,-30);
			if (fecha.after(calendario.getTime())){
				numDiligencias++;
			}
		}
		
		if (dentroCalificacion && dentroNota){
			double nota = Double.parseDouble(texto);
			mediaNotas+=nota;
			numNotas++;
		}
		
		
	}
	
	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		
		/*
		 * En este evento establecemos a falso los atributos que llevan el estado 
		 * del procesamiento. Ejemplo:
		 * 
		 * if (qName.equals("elemento1")) dentroElemento1 = false;
		 * 
		 * Alternativamente, quitamos la cima de la pila:
		 * 
		 * pila.pop();
		 */
		if (qName.equals("diligencia")){
			dentroDiligencia = false;
		}
		if (qName.equals("fecha")){
			dentroFecha = false;
		}
		if (qName.equals("calificacion")){
			dentroCalificacion = false;
		}
		if (qName.equals("nota")){
			dentroNota = false;
		}
	}
	
}
