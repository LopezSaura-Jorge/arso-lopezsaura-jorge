package stax;

import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;

import javax.xml.stream.XMLStreamReader;

public class Ejercicio1_4_Stax {
	
	public static void main(String[] args) throws Exception {
		int numDiligencias = 0;
		boolean dentroDiligencia = false;
		XMLInputFactory xif = XMLInputFactory.newInstance();
		XMLStreamReader reader = xif.createXMLStreamReader(new FileInputStream("ejercicio1-3/ejercicio1_3.xml"));
		
		while (reader.hasNext()) {
			int evento = reader.next();
			if (evento == XMLStreamConstants.START_ELEMENT 
					&& reader.getLocalName().equals("diligencia")){
					dentroDiligencia = true;
			}
			else if (evento == XMLStreamConstants.START_ELEMENT 
					&& reader.getLocalName().equals("fecha") && dentroDiligencia){
				SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
				Date fecha = null;
				try {
					fecha = formato.parse(reader.getElementText());
				} catch (ParseException e) {
					e.printStackTrace();
				}
				Calendar calendario = Calendar.getInstance(); // hoy
				calendario.add(Calendar.DAY_OF_MONTH,-30);
				if (fecha.after(calendario.getTime())){
					numDiligencias++;
				}
			}
			else if (evento == XMLStreamConstants.END_ELEMENT
					&& reader.getLocalName().equals("diligencia")){
				dentroDiligencia = false;
			}
		}
		System.out.println("El numero de diligencias encontradas es: "+numDiligencias);
	}
	

}
