package stax;

import java.io.FileInputStream;


import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;

import javax.xml.stream.XMLStreamReader;

public class Ejercicio1_5_Stax {

	public static void main(String[] args) throws Exception {
		double sumNotas = 0;
		int numNotas = 0;
		boolean dentroCalificacion = false;
		XMLInputFactory xif = XMLInputFactory.newInstance();
		XMLStreamReader reader = xif.createXMLStreamReader(new FileInputStream("ejercicio1-3/ejercicio1_3.xml"));
		while (reader.hasNext()) {
			int evento = reader.next();
			if (evento == XMLStreamConstants.START_ELEMENT 
					&& reader.getLocalName().equals("calificacion")){
					dentroCalificacion = true;
			}
			else if (evento == XMLStreamConstants.START_ELEMENT 
					&& reader.getLocalName().equals("nota") && dentroCalificacion){
				String notaTexto = reader.getElementText();
				double nota = Double.parseDouble(notaTexto);
				sumNotas += nota;
				numNotas++;
			}
			else if (evento == XMLStreamConstants.END_ELEMENT
					&& reader.getLocalName().equals("calificacion")){
				dentroCalificacion = false;
			}
		}
		System.out.println("La media de las notas de las calificaciones es: "+sumNotas/numNotas);
	}
}
