package xslt;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class TransformacionPoema {

	public static void main(String[] args) throws TransformerException {
		
		String ficheroEntrada = "xml/poema.xml";
		String ficheroSalida = "xml/salidaTransformacion-Poema.xml";
		String transformacion = "xml/plantilla-poema-Imperativo.xsl";
		
		TransformerFactory factoria = TransformerFactory.newInstance();

		Transformer transformador = factoria.newTransformer(new StreamSource(transformacion));
		Source origen = new StreamSource(ficheroEntrada);
		Result destino = new StreamResult(ficheroSalida);
		transformador.transform(origen, destino);
	}
}
