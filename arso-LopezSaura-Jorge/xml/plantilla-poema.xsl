<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="xml" indent="yes" />
	
	<xsl:template match="/">
		<html>
   		<head> <title>Poema</title> </head>
   		<body>
         	<xsl:apply-templates/> 
   		</body>
		</html>

	</xsl:template>	
	
	
	<xsl:template match="poema">
		<h1><xsl:value-of select="titulo"></xsl:value-of></h1>
		<h2><xsl:value-of select="@fecha"></xsl:value-of></h2>
		<h2><em><xsl:value-of select="@lugar"></xsl:value-of></em></h2>
		<xsl:for-each select="verso">
			<p>
				<xsl:value-of select="." />
			</p>
		</xsl:for-each>
	</xsl:template>
	
</xsl:stylesheet>